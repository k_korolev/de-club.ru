import axios from 'axios'

const getApiData = (route = 'dc/v1/params') => {
    const url = `https://de-club.ru/cms/wp-json/${route}`
    return axios.get(url)
}

export { getApiData }
