import { createApp } from 'vue'
import App from './App.vue'
import 'swiper/swiper.min.css'
import 'swiper/modules/pagination/pagination.min.css'
import './assets/sass/style.sass'

createApp(App).mount('#app')
